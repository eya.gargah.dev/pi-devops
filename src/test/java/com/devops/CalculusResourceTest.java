package com.devops;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import javax.ws.rs.core.Application;
import static org.junit.Assert.assertEquals;

public class CalculusResourceTest extends JerseyTest {

    @Test
    public void testSum() {
        final String result = target("sum/4/3").request().get(String.class);
        assertEquals("7", result);
    }

    @Test
    public void testMultiply() {
        final String result = target("multiply/3/3").request().get(String.class);
        assertEquals("9", result);
    }

    @Override
    protected Application configure() {

        return new ResourceConfig(CalculusResource.class);
    }
}
