package com.devops;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

@Path("/")
public final class CalculusResource {

	@GET
    @Path("/")
    @Produces(MediaType.TEXT_PLAIN)
    public String welcome() {
        String result = "Welcome to the best Calculator App!";
        return result;
    }
    
    @GET
    @Path("/sum/{a}/{b}")
    @Produces(MediaType.TEXT_PLAIN)
    public String sum(@PathParam("a") int a, @PathParam("b") int b) {
        String result = Integer.toString(a + b);
        return result;
    }

    @GET
    @Path("/multiply/{a}/{b}")
    @Produces(MediaType.TEXT_PLAIN)
    public String multiply(@PathParam("a") int a, @PathParam("b") int b) {
        // Note, both compilation & tests will be run by: mvn package
        // String result = Integer.toString(a * b * 3); // Wrong Implementation Example, will fail CI/CD Pipeline during running TESTS
        // int x = 'some_string'; // Wrong Code Example, will fail CI/CD Pipeline during running maven COMPILATION

        String result = Integer.toString(a * b);
        return result;
    }
}
